<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'aluraviagens' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<C2:9L9lr%HVRRo@*!$~xiX3p;1tpXQSP<-!*l^X!XxXG3! :c)6zBeVQ7Gh&|hD' );
define( 'SECURE_AUTH_KEY',  'V87uNI8*qn7A+{a6F7CAUz4afanJPr5MS#u>Za1VL`3X~hWKgo=*%XJGudaj]+vw' );
define( 'LOGGED_IN_KEY',    'S +@&3[Vt+0_ou*[fW1XjCv2|Ov{xa0?EH0!AzoCvE_*gTvH?NSf8L;E(,6V09s5' );
define( 'NONCE_KEY',        'w Oeo/dOY,gMr^(Sf`YMp9f:>[rfeH|F|KC]Wxa+y(L4|BU9uGyc&Y>@bk5L-h]7' );
define( 'AUTH_SALT',        '2nA^PV[Tt6/Z7g1g{<:/@-O^FBB?8I(?=lqbPn*-@Np~9-$Csh{s69-_uRq9#Qn[' );
define( 'SECURE_AUTH_SALT', 'EH2Mi <Z6|O$C=LDArEz|ZnoT~8O31QA|M.k4i=l{86aWIQ>!t$&8tnlz6W|LUWA' );
define( 'LOGGED_IN_SALT',   'E=hQ5jZBw0~2L=JG%64Xr^Gj.=f(Q<0O>m7:5wK,mdW9#U6NL[;.<a{60?gC:rh7' );
define( 'NONCE_SALT',       'kY{=iX@b-0<y,k}#yqF@<3n0i=vNJzXeT+<P;z`;T1u4pI(ciI;9|RW2K(~jz#O#' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
